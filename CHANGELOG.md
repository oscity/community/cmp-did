# [1.43.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.42.0...v1.43.0) (2021-09-28)


### Bug Fixes

* Contribuitor fields ([f34e6cf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f34e6cf01a437eb2b015d013c4278afeeb5cec07))


### Features

* New Version ([918437d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/918437dc9fee0c36ee3110a7b941447eb17bbcef))

# [1.42.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.41.0...v1.42.0) (2021-09-28)


### Bug Fixes

* Change chart titles position ([1d3411a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1d3411a7c200dd6098483b3e544e10017f88a726))
* Change colors to graphs in analytics page for unicef ([e3139d0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e3139d0420eda8bf30811cbcfdf2aaca2928c513))
* Change graph from course to course type ([765fc7b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/765fc7b9bc80901a25bce5fa9dfd246536ce9684))
* Change map position ([22c110a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/22c110a15c3fd0e94a32841afb260f4ae89f9e4f))
* Hide login links and texts when loging in ([d1d1af6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d1d1af69bcf38bc4e6bc02cb7d752cfa29da0a4c))
* Logout autenticar ([68f4b89](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/68f4b89b76ad18a532b1d4b22390dc48adfd15be))
* Missin i18n translations in map ([a93d54e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a93d54e7f121938e946216e982a036e761983c54))
* Remove 'is_revocated' from roster ([840232b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/840232b93a7a97d9620bcbf683abac450b4c4481))
* Remove console.log from component ([5ce69c2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5ce69c2711788a26016b96aef84250631e820367))
* Remove shadow box for v-stepper in Register Misiones ([49b3145](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/49b314530bd41c4eff0bd37e6d51138bb4755993))
* Validate autenticar error ([ebcd31e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ebcd31e2e2505aa177bf32f5177678fd84e36384))


### Features

* Add alert for missing data in unicef analytics ([b74e284](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b74e28423ae87fe7198a8271b8a7fa5fd6bba4a8))
* Add i18n to analytics ([be27d2e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/be27d2e396495afd96cd81e4345130a2f59e974a))
* Added d3 and highcharts for Unicef analytics ([279bd65](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/279bd65ce1809329990486b2bce5a551c36ab80c))
* Added map to Unicef Analytics ([b793f88](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b793f8840b3e28293bea0853f7135e3ad01d7732))
* Change sidebar style and add analytics link ([adc3c0b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/adc3c0bd63fd860d8709f995abb42b973225291d))
* EsLint ([58239e3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/58239e3db4a2b335bea5a32a5ebb7845112adf09))
* Pie, Column and Timeline Chart for Unicef certificates ([11b97af](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/11b97af5571c97b6f4bf6b8148263ce6b9f9d057))
* Refactor firebase service ([bcc7a94](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bcc7a948ddab956d08a7353b8bd281af7c369b09))
* Update Vuetify to version 2 ([8dde4a1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8dde4a1346a1adfa172b0bbc887adbde1ee0d567))
* User level up ([c42372b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c42372b8f9db93ab954940db83502a6f7f28f839))
* Validate loader ([bbb2538](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bbb2538167d1ea0bd960442d7756030f8625aa2e))

# [1.41.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.40.0...v1.41.0) (2021-09-22)


### Bug Fixes

* Add jest ([46272b6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/46272b6b23a98f0061ce9640c89ee85ac4ec5ec7))
* Add some i18n fixes ([4bfb033](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4bfb0330d0199ce2c3cc310cfa47fe1aff3777b0))
* Added authorized column and i18n fixes ([a091385](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a091385d69fea8b128d556f3322868a6a3d252ea))
* Added i18n missing translations for UNICEF ([009355e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/009355e33138c643fbdf470df601123600b87124))
* Changed images for mobile version of Misiones and changes in footer ([78ce65e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/78ce65ed10fd97f3bd52cf6720a8f40618c0c569))
* Default locale i18n ([16ae376](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/16ae37644068990733c81bd5e18697b61ef18f56))
* Delete firebase json ([da07ac6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/da07ac6b78bb0fe51112788794085bc64b268551))
* Delete firebase rules ([7f1b440](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7f1b4401fb1b821995058f76e9574bdfda682448))
* Es to EN ([9007ee9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9007ee9ce4f1098f24725d7db9089dc464127e7d))
* Fix Login Test ([e27bafb](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e27bafb0d0c31fa232eef15fef366094f18d94f0))
* Fix Login Test ([e1799c4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e1799c42051cab5c0a0b0930cd555481f281b09b))
* Fix tests ([2433ae1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2433ae1f5ab9ce62a784a5513e7e08c6673edafd))
* I18n locale bug for Misiones ([434509d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/434509df68111311e1d0fdb21033a4ee77c879d2))
* Import componentes ([d2d4526](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d2d4526df68e3e47590563686ee266e7cb0b3789))
* Import Nec Components ([5b1130b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5b1130b8017482ca73f5868fb892b2624d229ac3))
* Missing i18n translation in DataTableCertificates ([0374112](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/03741124c7954abd6a532a2ca125e4536704c7ae))
* Missing i18n translations in Login component ([3a996e1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3a996e1f31062d741a6a7021011cd5ccc49402da))
* Refactor edit texts landing code ([d54c6c0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d54c6c09e820953f5de439d1248586a7a48e8c8f))
* Remove comments ([16510ea](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/16510eaee00a62182189d06158c3b1a6110eadce))
* Remove components on landing page Misiones ([2f142cd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2f142cde1a9ec42300344c10ebb949b0f7db0157))
* Remove white space in various pages ([0e8ace4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0e8ace4cd9086c78666f1e76917ccf27461eb522))
* Replace v-template for template ([7382135](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/73821356871717b3c0ad16fb68b18c9c88162f84))
* Send certificates if doesn't exist ([cc25174](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cc2517423418aab5ba38422aa7cb36d1ed66f2fc))
* Sidebar i18n ([31ff37a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/31ff37ae9bee2c56aa9281af7a1b12608c00e6e7))
* Solved bug with dates when importing data ([07e16f1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/07e16f1ea5b56686de9070c267b011d1745726f6))


### Features

* Add i18n to landing editor dialogs ([4698ed0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4698ed0ee192ca0c547c8862d8bb8fa2feeac3f5))
* Add i18n to landing editor dialogs ([1b31a69](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1b31a69e75f478da2bd3cf749ce7f39415673887))
* Added v-dialog for loader when sending emails and i18n fixes ([afc97b1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/afc97b14a3b47bd8f4b1ae59f8cbff3b20763012))
* Better UX on Alta Contribuyente ([3b088e4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3b088e473254f2387cfc035b75eb7e5dce427baa))
* Enforce user to return to complete data ([f2ecd5b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f2ecd5b82dbf9e50f30d18d1f78f110d32273e85))
* Files firestore rules ([da3c7f1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/da3c7f172461e950aa98b1b9e06e41ec149e2464))
* First test ([0c4cba9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0c4cba980263de4993f9decc5f31c77580605bd4))
* Merge dev ([49a9241](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/49a92413d193ea03c3600d7c17cd877421f70489))
* Nombre de Certificado ([1779b0f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1779b0f81f2ea2ac7a186c954e4657c1a4497293))
* Nuxt config ([76a8544](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/76a8544dcbf83d928f3d3de615faffa33d9e7a90))
* Real texts to Alta contribuyente ([d8871a2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d8871a26ea148e9c8623dfc6efafc2d8118e7426))
* Setup firebase emulator ([c906e87](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c906e8776ad4b3843cc763552803da4fc5914f2b))

# [1.40.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.39.0...v1.40.0) (2021-09-13)


### Bug Fixes

* Change i18n es.json ([f35ad71](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f35ad71136f67311f38fd3c2fb4f602cc64d81e0))
* Images for the authorization page ([5b493be](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5b493be977ab8e0186096ca8e921e17075ad3ff0))
* Loader position for authorization page ([8526fc3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8526fc35061816793601e41d9d8e87d6f0cf3a52))
* Minor Fixes ([711a40a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/711a40aca47d956498a4ba98df4e790499afeec1))
* Remove swal for succseful email ([a5c86c0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a5c86c0a914b71509a1718bf7ff46d8776a3cbd8))
* Responsive image width in mobile view ([64f12d9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/64f12d9e3171885180cf933b969e3e5d688dfdec))
* Show error messages in validate email ([3188a52](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3188a52215c81ac8d7cf96ebc91aef72534b8ac6))
* Text changes ([780388d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/780388d508feae95022cfb16a2e3ae9cc788906e))


### Features

* Add error validation to email tokens ([7d77597](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7d7759751bff098e2debcdd9c2a3fd003aaf8291))
* Add group email functionality ([967d21e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/967d21e919f310083ce3231cdca02c0f5d205d42))
* Add loader to validation of token ([1145854](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1145854564e223af743868fcdce6f8df40aebe91))
* Create and validate token to authorize certificate ([c504958](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c504958e1f78ebc8e0d48075abb35be06d50a872))
* Disable send email when emails are being sent ([50e1ed2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/50e1ed274e9a1ccc1a137d9791f222e43aa316e6))
* Error page if token can't be validated ([1abeae0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1abeae0036a950e0f4f51674896958c15cbfc38b))
* Functional radio-buttons and authorized-certificate page ([45c10ee](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/45c10eef975d138a707193e4e4133de2157ff05b))
* Send test email ([00fab0c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/00fab0c32a6c65923453cfc2379fc1d7ee0f9e09))
* Send test email ([ef61e33](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ef61e33c32acf04c0eadd46a05c0cce044f8ed6a))
* Styles and i18n updates ([0b67a8a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0b67a8a5b2d163c2ac6a6d32044f391d7656d1af))
* Update v-alert with user email ([267b54f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/267b54f8e45f281a94a94ea54fce7b9c761ba63a))
* Validation to send emails and certify certificates and style changes ([64d5bfe](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/64d5bfe5959b8b4b66351b9c46f72a4e756f0ddf))

# [1.39.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.38.1...v1.39.0) (2021-09-09)


### Bug Fixes

* Validate null value ([e5869ae](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e5869aeedc8c9c61641d4784422ca58c549f4367))


### Features

* Time sweet alert ([42f0c15](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/42f0c15f54bcc5606421f4c9093f6a2df007b1fa))

## [1.38.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.38.0...v1.38.1) (2021-09-09)


### Bug Fixes

* Object empty validation ([4e19c13](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4e19c13aaf8a7e2b0db03c024113d9c75b556363))
* Validate null value ([57d447c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/57d447c7150c46b80b604e31c35ef13e4a7e7b63))

# [1.38.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.37.0...v1.38.0) (2021-09-09)


### Bug Fixes

* Brought back create CcontributorCert when complkete data ([d221f30](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d221f30ec428de48c35ec1e003b6a925631535e1))
* Create certificates only if they doesn't exist ([90ec9be](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/90ec9be84a47cdd271d0f6c5ba3187e871752c2e))
* Remove console logs ([5c6987b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5c6987bbf8fbc5de09d401da774c34a41c0841cb))
* Show 'Alta' only if user is level 2 ([f091a8d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f091a8d76a112e81e744da93f3565571c009d63a))
* Show 'Alta' only if user is level 2 ([2f151e0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2f151e092812e70159c7f28b00bd33dc7af04cda))
* Show specific errors in login ([9ab7252](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9ab7252ec9b52043fee171a3eea07dbcc73414cd))


### Features

* Crate Alta Contri when Filling form ([099ad38](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/099ad38067f26153ad9c93868ae621f3de6364b6))
* Error image from db ([f8503a7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f8503a747b6f0f9e4e1eeff6b865d4be35b4cceb))
* New CMP Version ([651a88e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/651a88ef4cddf018689b5415f2c825356d86563e))

# [1.37.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.36.1...v1.37.0) (2021-09-09)


### Bug Fixes

* Certificate dni complete - Alta contribuyente ([cf576e7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cf576e7f10acb3752724dab527a00d30dbfef142))


### Features

* Add validation checkbox ([c1d6352](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c1d6352d3fdc0dd88624e56b0efa0f41d66e7adc))
* Remove cuit input ([a5e1013](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a5e1013e9dbe476e0da33444bda2e9af8c99417e))

## [1.36.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.36.0...v1.36.1) (2021-09-08)


### Bug Fixes

* Remove disabled cuit field ([cde6526](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cde6526fe60c207d2192e44d8b9742c66ee894fc))

# [1.36.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.35.1...v1.36.0) (2021-09-07)


### Bug Fixes

* Fix Register Test ([a8302f0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a8302f0b027af45c3fa0ee322773944b87d893c1))
* Step 2 - Disabled cuit ([2df6082](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2df608290048f8309f66bea44ba1051a92feaa5e))


### Features

* New version ([70b770a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/70b770ad44236a50aa9beef50b9c50929126827e))
* Show hour in create data ([8c8f71c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8c8f71c21c0ece6bfec4820725a0b96f5bbec6a6))

## [1.35.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.35.0...v1.35.1) (2021-09-06)


### Bug Fixes

* Bigger logo image in header ([c32d20e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c32d20e91f2f7f451eb18f296b5d9b61a3ce1f90))
* Change class for logo ([e0a3326](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e0a3326e41d0df230ad545beffd81fb2e6d2adfe))
* XL change for bigger screens ([ad3e764](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ad3e7642ee9894bbace2ac05d3f1429f715b5bc8))

# [1.35.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.34.4...v1.35.0) (2021-09-06)


### Bug Fixes

* Reduce timer when creating certs ([93bd057](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/93bd05724b627557c31d1b8c09a78e0fa2d15ea7))


### Features

* Not used style ([708ae5b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/708ae5b14c2262c582dc09ba99170ec155ab8b6c))
* Texts Argentinian Style ([acd16db](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/acd16db2dc39f85c1c0487c0d4ed5fae061ad816))

## [1.34.4](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.34.3...v1.34.4) (2021-09-03)


### Bug Fixes

* View Id certificate ([d8ba2a6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d8ba2a677a2e46369bfdd2a648d5cde4fcfd2b43))

## [1.34.3](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.34.2...v1.34.3) (2021-09-03)


### Bug Fixes

* View Id certificate ([49e0a08](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/49e0a0867aa660dfbdc192ed633adbae3a541444))

## [1.34.2](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.34.1...v1.34.2) (2021-09-03)


### Bug Fixes

* Sentry only in production ([fed58ea](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fed58ea48188d2c5494b5b7ef6641c832a4a45ad))

## [1.34.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.34.0...v1.34.1) (2021-09-03)


### Bug Fixes

* Get certificate in Avatar menu ([3f95253](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3f95253c319739239814e8f7dd2796029d3ed227))

# [1.34.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.33.0...v1.34.0) (2021-09-03)


### Bug Fixes

* Change procedures texts and modify responsive header ([479bf32](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/479bf325f1338e08ef3abc974785bc42bd54f988))
* Delete comments ([0e6901d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0e6901d6b68641b25bb27e7ef80068ac0ecc4cc6))
* Edit 'Alta de Contribuyente' procedure descriptions ([6b10067](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6b1006788d6191c01f0997dfc1154334d8fd470f))
* Remove commented loader ([0088979](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/00889793f9274d5fc3b99620ddcfcd17e86bfb78))
* Remove v-show after v-if ([fe6fdc0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fe6fdc0a1f13a9dd4fdc50b93a86598ecf56d82e))


### Features

* Add confirm password input, add view password text and link to download app in playstore ([83cb41c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/83cb41c5b693cc4206a802760ea272f6d9785a48))
* Add sentry to logrocket ([ee732a8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ee732a8e66eb3cbbb1476bc19364deac790dca3e))
* Conditional rendering for alta de contribuyentes ([e3cb864](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e3cb864ee7a6f80c70b234ff49e0caccf506526b))
* Configuration logrocket - Misiones ([c39d942](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c39d9425de38f54674cf8e35578ef946d4a11e68))
* Logrocket config ([788cd62](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/788cd629d34a8135f0d8a7e1e6965dd35e8d38ee))
* Remove header links and searchbar items if registration is not completed ([b55f1b1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b55f1b1c52e31d6412aaa95695a0e1eaf31bd52a))

# [1.33.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.32.0...v1.33.0) (2021-08-31)


### Features

* Redirect to step 3 when verify account ([3cba0c2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3cba0c29fc2cbe2ee4619965d7069889d9faff18))

# [1.32.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.31.0...v1.32.0) (2021-08-31)


### Features

* NivelCidi sent to Rentax and Fix ContributorCert ([1e61be0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1e61be069b2354245d12754efe8e92d60eb00284))
* Version update ([d35fbb8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d35fbb829332488d7183fd5c98f5eeb0076f5b28))

# [1.31.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.30.2...v1.31.0) (2021-08-30)


### Features

* Add unicef to gitlab deploy ([64a51ca](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/64a51cae3e474f2fc043181798625f7d6909dd64))

## [1.30.2](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.30.1...v1.30.2) (2021-08-30)


### Bug Fixes

* Delete 'Ingresar Ahora' and 'Mis Tramites' buttons. Add Lucas' texts for procedures public view ([e393722](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e39372202809df6c0f8654d7b3aa6d7cffa87f3a))
* Remove access to register and complete-data pages ([2e94450](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2e94450e4dd99b0175cca30d473921e64f024b8f))

## [1.30.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.30.0...v1.30.1) (2021-08-30)


### Bug Fixes

* Error to show user data in step 2 ([49fc8f3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/49fc8f3f8a7b1911c400f975c7bf60d31bf74eb5))

# [1.30.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.29.0...v1.30.0) (2021-08-27)


### Bug Fixes

* Show correct message in register ([8eb9ceb](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8eb9cebd977673c75869af1e17672837544ff29a))
* Text in step one ([9daa2e0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9daa2e05d3cef43917dca8c3b8f8a2ce323f7f44))


### Features

* Validate Texts, accents and quit buttons not used and redirects ([c779a50](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c779a505c5af85eb646f6847f27727e0d231daf7))

# [1.29.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.28.2...v1.29.0) (2021-08-26)


### Features

* Remove console.log ([d873754](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d8737547f22657c543328fc60bcbb97bff80ec60))
* Remove ID Form, automate ID cert ([bb304ac](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bb304ac183bf730766fe3213a8ff5a12d51fc831))
* Responsive Header, texts fixes and console.log with CMP Version ([7939c33](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7939c3309a693130b5cffc9b0b5f46bcd8fe0ea7))
* Validate Default Layout Header when misiones-id True ([9eaa91f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9eaa91f4a33aff7f1868aadf908ff7304996beee))

## [1.28.2](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.28.1...v1.28.2) (2021-08-25)


### Bug Fixes

* Fix for collections ([cb3c08e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cb3c08ea097c84451c27dcf6850821819d4d1dde))

## [1.28.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.28.0...v1.28.1) (2021-08-24)


### Bug Fixes

* Redirect to step 3 or procedures ([b04e2bf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b04e2bf528245d58b0c041d729f1b7da439d2cf3))

# [1.28.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.27.1...v1.28.0) (2021-08-24)


### Bug Fixes

* Analytics error ([4fc7fc8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4fc7fc89315cf00fd6eadecf3c0fabf26fc8ab4c))
* Change instance validation for header ([d771d15](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d771d151c6f1dd4c6c678aea12ea52546db1b4d5))
* Change Procedures Sidebar links ([0966f86](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0966f86b3df5127855983858a1135346396969af))
* Change Procedures Sidebar links ([4638fb5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4638fb5c9f0869f65938a22339f0388763deb03c))
* Fix comparison of instance on default layout ([79e4a72](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/79e4a721718799a1e2f4f17249440edbb7af5d4d))
* Merge conflicts Dev with branch rentaxFlow ([01a202c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/01a202c3dffbad75aeb439f0c2877ca7a80e88ca))
* Realm missing error ([28caca6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/28caca69adbbe9053396854102ee63318f08893b))
* Remove 'DNI Cultural' text ([223bf64](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/223bf646c8b63dc1f00a0750b746b9a1753ebc8a))
* Remove 'DNI Cultural' text and loader ([a3da5d7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a3da5d74e8123caa6c14ec985565d41401eeb690))
* Styles for snackbar ([821731c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/821731cba8d1df20c34c32111fbab86085400c12))
* Verification email ([86100b1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/86100b1bac6cbd2ebc5616de107f9bba9a16cb32))


### Features

* Add public view for procedures and its sidebar ([e0ad644](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e0ad64438f785146113412f471725781b0d13a63))
* Add public view for procedures and its sidebar. Delete lorem ipsums ([21c3940](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/21c39406196033282bc73031d70b7815813d67e6))
* Catalogs corrected and rentax back to original ([f0aacd2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f0aacd2ba94e724b9658954cfb37ec81ba4fea7f))
* Don't show avatar menu if user isn't registered or doesn't have public_address ([4238a81](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4238a81cc54b98a390f40cfa1d4bef1236ea3897))
* Middleware - Don't show login and register if user is logged ([383edc1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/383edc18425954a75d53399a1f8f8d60f78cb019))
* Middleware - Send step 3 if public address doesn't exist ([91d6f41](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/91d6f41bbf0a296bbc26b75e646bd14d45f7da02))
* New logRocket id ([ae32def](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ae32deff1eb2cd1ffe604f1aec7eb848980465b3))
* RegisterAutenticar con nuevo estilo priorizando AFIP y validaciones Rentax de vuelta ([90721fb](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/90721fb2aa2e5028dde6aa42e66eca7e39a546d9))
* Remove step ([23d87ed](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/23d87edc489d7a83f06a988f4dfcc76c0c234786))
* Send step 3 or 2 ([8b059f7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8b059f75f86f3ff03f5987b2eb94daa3b51ec47e))

## [1.27.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.27.0...v1.27.1) (2021-08-20)


### Bug Fixes

* Email subject ([9509dc6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9509dc6157ec910f434133535f3c4e84bc91c26f))

# [1.27.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.26.0...v1.27.0) (2021-08-20)


### Bug Fixes

* Fix footer linear color. Modify Header and Sidebar by instance ([f0658ca](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f0658ca971d2212d0d181e033a6f1aaec46aae4f))
* New User Store ([c9689f9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c9689f9b454c796faf97d815e4797ea815d8117d))
* Remove console ([733e1db](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/733e1db99cff35ded27080eb3957064c3bcc59d9))
* Sentry & logrocket api keys ([f339426](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f3394263745728ce63757e30db2c2fefc1192204))
* Sentry test ([e4c9122](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e4c912234647e34958c318c903729dbfbb9309c1))
* Validation errors and server error page ([29e8e9a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/29e8e9a8039768b1995489dd21b648906179067c))


### Features

* Hide Alta Contribuyente on procedures poage when cidilevel 1 ([c86cb9a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c86cb9a9cd9c66e0e8ddfcb5d9b3bb9fc1a3bc02))
* Rentax Si Flow ([739ea90](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/739ea9085597119e00e07e1a16dbfe1a031e85ad))

# [1.26.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.25.0...v1.26.0) (2021-08-19)


### Bug Fixes

* Merge conflicts ([fc91ffd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fc91ffd53701d40cbe59dac8fc85e84bf24cd58d))
* Set user in login and logout ([b49ae1e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b49ae1ea4d92a658f670c5c08ed82d3c2453bbf5))
* User data ([04bb287](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/04bb287455de0e69cd22fce6a01fbeb6af18a956))
* User Header Bug ([f3dac5e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f3dac5eae77cfada57a93f3d32af5300465cf27c))
* User store ([82c3488](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/82c34881c78fc1c2bdf38720e4f0323cdcdf32a0))


### Features

* Add captcha to contact form ([c65e4ae](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c65e4aef3b04057ed3a3ea13be5e6d4451c1b524))
* Add level cidi misiones ([82ec49a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/82ec49ab416afecaa765855ae53514538dbd7d4c))
* Add links to avatar menu ([82dfca2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/82dfca2bd13fad4e3523f26f102a4962b60a2555))
* Flujo Rentax si y no ([31c5d62](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/31c5d62682760feb4b90a19ae6202c638479ffe6))
* Prep ADC Certificate ([124773f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/124773f14cc296c4bcb8869a770627cb5395a7ca))
* process env EMAIL NAME ([3e38156](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3e38156ad7d0f8ad36af6379197e02d719853b57))
* Rentax Flow Certificate ([8c8b140](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8c8b140bee466dad4d681a4fcd4380053a9e72b1))
* Rentax prep on form ([697a4df](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/697a4dfd433c620f48f7ebccfd2326206ad9a6f1))

# [1.25.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.24.2...v1.25.0) (2021-08-16)


### Bug Fixes

* Change what-is-cidi images ([e1746dc](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e1746dc5b80938328c338a78b75c249baf16c741))
* Delete console log ([e6a886f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e6a886fcaab9972df07cf6d2afd29276461b1ad3))


### Features

* Add header changes ([b60aa67](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b60aa677f0a2ecf811fb6d0a86078b640d9c59eb))
* Add procedures pages and alta-de-contribuyente form ([190aaee](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/190aaeeb7e183cf27ff362449a7c5de06059791b))
* Add responsive sidebar and functional searchbar ([a145350](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a145350d24f3bbc0968bd42c186b5a19e6d23e24))
* Filter on Domicilio corrected ([a2653ab](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a2653ab3fbcd48dc2d24558b0bfff84f74fb9395))
* Merge with dev ([2fa0445](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2fa0445c235d6dc28fadea8315da3f1241797a32))
* New design for register and lock double registration ([9e1a588](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9e1a5885823de632fa2e91cc77769623b4ef9fa1))
* Nuevo intento con aPi de rtentax en aws ([416cd92](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/416cd926bd76da00cc235837d64547b3be1a5a20))
* Rentax connection ([8556043](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/85560435a088148048792926a0d679d019f2ffdc))

## [1.24.2](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.24.1...v1.24.2) (2021-08-09)


### Bug Fixes

* Change title Misiones certficate ([e66f083](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e66f083d62c98ca4f828934bbee322a75b60bd38))

## [1.24.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.24.0...v1.24.1) (2021-08-06)


### Bug Fixes

* Type 'string' is not assignable to type 'boolean' ([560fe8c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/560fe8cf453c5400eb791e36a9bc5dc20ad941d6))

# [1.24.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.23.0...v1.24.0) (2021-08-06)


### Features

* Add loaders ([541da0a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/541da0a15802a99ef09b7358823f3c0eb12c363f))
* New styles for blockchain modals ([1735362](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1735362d923ea0ab136b282a08655f7fed1685df))

# [1.23.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.22.0...v1.23.0) (2021-07-19)


### Bug Fixes

* Change header color ([c4df233](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c4df23365147d190a52406ab1cc537d1fd12fd76))
* Change the background modifier button for hero section ([eed0ca0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/eed0ca0ee5cc603c28bc8ea11937a8a3739fb448))
* Correct editable text that desappears on delete content ([fe5b5f4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fe5b5f451745f98325b0979e4bb7e681f0f378bc))
* Delete comments ([6f30f0c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6f30f0c4fd906a8e7e9366b9e183e1fc7191e2a8))
* Delete Nuxt favicon and add loader before load landing ([4ea5d5c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4ea5d5c8a14f501fdbb35c245c83df320fd285f6))
* DownloadWallet also as Mdq and Berisso ([42dedf6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/42dedf63e385dec4020d4ec7e1ae51648c4285b9))
* Restored Complete.vue version; Changes not needed ([a8acbec](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a8acbece9e5a774cf9902f83956a48c994df3189))


### Features

* Add 3 new pages to header, gradient to icons, styles fixes ([86d8d7e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/86d8d7e2fee22918e453c7462aba1bfbfbade7f9))
* Add buttons to change header and footer styles ([797c209](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/797c2090b871a32afbd1661f5edad822a451f68e))
* Add change hero background functionality ([309e9ea](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/309e9ea0009892a362fa6e5824c6d463abf6d9f1))
* Add feature to change reminder image and fix buttons titles ([f96ae6c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f96ae6cc4a223a99a262e575a8d7e8fe5c5c5390))
* Add footer styles, refactor fonts implementation and works on responsive design ([6f32fdd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6f32fddac8d762df882616ad08ee4b1782ba5263))
* Add functionality to change images in landing ([6f21130](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6f2113075093e01e205a4ddd6d971917e52750c9))
* Add landing UX styles ([b8ae485](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b8ae48568923173048760d2a1f9f26c5472d25bd))
* Add linear gradient to card icons ([b0fef6e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b0fef6e1508f4f498169e7b7c496a54b6326d2f2))
* Add linear gradient to card icons ([a537e94](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a537e949a8e31610c38d3d8bf0572cbeebba7576))
* Add loader when editing text on about section ([ba6042b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ba6042b1a0741bbce352a19c7dd718cfa395c8f7))
* Carga de datos para Ordenamiento ([2c82b8f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2c82b8f0333b6fdaa62bd5d16405c2c48b187209))
* Dervinsa Changes and searcher ([af0373e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/af0373edaea8a1f8fb1089de4ca34e7fae49009c))
* Escuchar cambio en firebase para public_address ([daf25bd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/daf25bdf2dad55eba14ffa423bdd24f41d1b407a)), closes [/firebase.google.com/docs/firestore/query-data/listen#web-v8](https://gitlab.com//firebase.google.com/docs/firestore/query-data/listen/issues/web-v8)
* Guardar user en store ([33ad7e5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/33ad7e5977fdbd123dada9c12110384390f0f35a))
* Make 'About Title' and 'About Content' editables ([6d20583](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6d20583c13dce16438b9f3d98424742583fb2513))
* MErge dev ([5cd94a1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5cd94a160f66e82e5ebc5b171de8ae4902e9ef2c))
* Misiones id same with Mdq and Berisso ([a5eabe4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a5eabe42f6c583be3a3f2202b148da4cf5aa345c))
* Modify Steps to receive password 4 Mdq and Berisso Logins ([0d65cb2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0d65cb2a5f1d741ce903d1a397d6189be4862d17))
* Mostrar modal de certificación por más tiempo ([1b4e61b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1b4e61bbf12c027d1268a200c82b834c183d53b8))
* Nuevos campos en carga de datos ([b62ef9d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b62ef9d8ca8c92d0584e97eb2c2c4b34765c3ce8))
* Quitar dependencias no necesarias ([e170196](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e1701961df5f54af7492a2beb75b887e48dacc6c))

# [1.22.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.21.1...v1.22.0) (2021-06-10)


### Features

* Deploy Dervinsa ([cc77f10](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cc77f10bc2776665485e134e45b6a8ab4a95c9f9))

## [1.21.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.21.0...v1.21.1) (2021-05-27)


### Bug Fixes

* SideBAr ([8b866f9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8b866f9e75d77337afc153fc0d9c430c56fdb59c))

# [1.21.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.20.0...v1.21.0) (2021-05-26)


### Bug Fixes

* Not test on Logo ([86d8ac1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/86d8ac1833649048f71b2060a8bb459af2e7fbb0))


### Features

* Ajustes de estilos para theme de vuetify ([685f1ad](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/685f1ad1fb4c03d721137ad1539c8bb6d1b1077f))
* Campos para certificado ([ac131e5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ac131e53493253d3b6357c4abd20dde50ce9af0b))
* Campos para certificado ([10d5863](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/10d5863315b082816641ee6b52aea9a3b7ff4e89))
* Configuración para deploy Misiones Fondo de Crédito ([962a74d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/962a74d886b47f503b4ddd7cf141f3f0d2fa4647))
* Setup Misiones ID Cert ([bc25b76](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bc25b761f2127fb6ad60ec65d1f73c6580881981))

# [1.20.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.19.0...v1.20.0) (2021-05-19)


### Bug Fixes

* Not pushing to / when register ([1442991](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1442991b4d17f874db8a37473439d32832cc93e7))


### Features

* Hide Uport ([0bcd40b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0bcd40b2f6cacfd1063a45f0679ab372422ed813))

# [1.19.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.18.0...v1.19.0) (2021-05-18)


### Features

* Reautenticar usuario para actualizar información sensible ([b80cd9b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b80cd9ba3d840d231af8bf675acfafab9496bc6f))

# [1.18.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.17.2...v1.18.0) (2021-05-17)


### Features

* Certificar al completar datos ([d551185](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d551185ea9dab7eea3fec8efa521219f274b0256))
* Loader ([de6f93f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/de6f93f4a535f30b9ad2aff3c61724ec2df98e21))
* Redirect to complete ([c7e154a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c7e154a127b244df3346924343a28b4c3ac6eaef))
* Registro mediante autenticar y Login por medio de email y contraseña ([517c985](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/517c985cd4d52bedd31a552ee39f3f17610e420d))
* Registro por autenticar ([df74607](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/df746076de1a8985d92deef138698880d4db936d))

## [1.17.2](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.17.1...v1.17.2) (2021-05-11)


### Bug Fixes

* Validar botones para iniciar sesión ([0aadd8b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0aadd8bc813786c254d66621f46fbc151bd638ba))

## [1.17.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.17.0...v1.17.1) (2021-05-10)


### Bug Fixes

* Remove reino ([64d0cf2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/64d0cf23e921c64405c3b6d437b58f47bfdf2c24))

# [1.17.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.16.1...v1.17.0) (2021-05-10)


### Bug Fixes

* Loader ([44b8e1f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/44b8e1f30ce929d26373faaf0af67568061d81a7))


### Features

* Login Metamask ([98ad9a9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/98ad9a91b3773285318f8eee5ff5e7da9b7d992e))
* Login not tardado ([afa82e0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/afa82e093cff13f47aacb68c53252bf6591f4e3c))

## [1.16.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.16.0...v1.16.1) (2021-05-10)


### Bug Fixes

* Remove computescanning ([e7b17ac](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e7b17acc47e98d16ff324c862af746bb07dbea2a))

# [1.16.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.15.0...v1.16.0) (2021-05-10)


### Bug Fixes

* Bye Console and timepout con Complete Register ([0b215ef](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0b215efb7fca72d6102ff4a5e04f187241a22e74))
* Limpiar localstorage al inciar sesión en autenticar ([4e51797](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4e51797f4133dbf1a8efd52cff8918d418b1aa69))


### Features

* Cambio de color al menú y form del footer ([1013766](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1013766d1fb509066713d27e55c16511b550999c))
* Cambio de color en header y footer ([0dd8047](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0dd80473781735f2f276d990102c2bcdc7f3d8d4))
* Cambios de texto ([1847641](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/184764112ab702168dde0c3fba6a165a8c5a4c8e))
* Complete Ux flujo to receive ddrees from autenticar ([5339f3b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5339f3b565b31f1be3da32d69a15ee2c004fe157))
* Completed Register noArg ([1999153](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1999153d3341f900458d9e8f615c2a4ac59f2dbd))
* Estilos para snackbar ([e77ed4d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e77ed4d26386d2a49fa8d89b675dfcc9f6ec3d85))
* Getter para obtener usuario del store ([d5d0932](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d5d093268c4d0636f47fd273c5074c5aa8105f62))
* Icons autenticar ([0833899](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/083389968a3af1cd0918d1d312194cae9dc73b49))
* Login con wallets ([706e0e7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/706e0e7ebf422de0d40f6870cc3a376ebe40699a))
* Login UX ([e957302](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e95730249b604dc869c9d6433b6711e3bacf1fe5))
* Mostar opción para desconectarse de Uport y coinbase ([b0aed24](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b0aed24fbd154d7d1e5972c1ff69818deeea098c))
* Página para recuperar contraseña ([104ebf5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/104ebf599cae3ad58422a2560ad65112b7ecacd1))
* Redirección a página principal ([b7c5013](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b7c5013304e9977e766426822823c66b0c6958d0))
* Register on Steps ([54e5587](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/54e5587fb79e30c12108a0593e666da3cc6b7dcf))
* Register PageCorrectly ([aaf33a1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/aaf33a18d598ac219d21b7a0029dfb3244b65c01))
* Remove console.log ([de8d6bf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/de8d6bf03450b472ac826ef9ce3736ab8e0345ff))
* Sidebar autenticar ([7d93c0c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7d93c0cfba83aef22ab5bece270d8f9a7f77eb2b))
* Styles header ([a437039](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a437039f9330abedb44727e68984d22728179410))
* Ux Completed Still Auto Certify ([9cc3039](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9cc303943397404c53a76a7e6fe51cc05d5fc216))
* Ux para login  - No argentina ([6ef442e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6ef442edaaa25ac25b13720e0c9af15b286aef6b))
* Ux y actualización de usuarios ([3909b79](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3909b7993a9178c29d54d3d92478b3c5b2e56621))
* Validaciones para login con wallet ([6f44a9b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6f44a9ba309c607e4dd5983954267b77c24ad216))

# [1.15.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.14.1...v1.15.0) (2021-04-21)


### Bug Fixes

* Ajustar margen de slide principal ([066ae7b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/066ae7b7650d64ad1b799f958cc882d8f783ab00))
* Campos certificados ([2580258](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2580258530ce5e76e44886b29346e0e26d5d189d))
* Campos certificados ([5894a3d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5894a3d4bbda4fb7aa75388aaae298a0b0a9d824))
* Campos certificados ([99b2565](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/99b2565f008ec78cae0ab2fe9f96f0bd8e118fe9))
* GCP para misiones ([9897f2f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9897f2fab38d4c70fd684ff47cd9fd4f88e0de26))
* Renombrar varibales y mostrar datos solo si existen ([b65eea0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b65eea09047f301366990ed7ae83abab69ef5698))
* Validación para página de register ([2e89ea8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2e89ea8dd7c522dcf88ee7d8c54275e2f533a3c6))


### Features

* Agregar renapem configuración ([3076af8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3076af8d241630fa996ed19f521c25cb5234532a))
* Cambio en header para autenticar ([a30a1c6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a30a1c6809bf2601e6ed54762e08a2b744308daa))
* Campos correctos para template ([8ea780e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8ea780e6ab3ead5926d8e333a465f226a157cd4c))
* Color dinamico para header ([92837e4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/92837e4819941b4545c6fffeee6141b26030b414))
* Creación de csv para certificación ([a619874](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a619874627cdcffbe955dc226806df3426f71f1e))
* Keycloack ([9a71b68](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9a71b689f5b0592117bb3d9a90ad1f68da872c52))
* No correr pipeline en dev ([ac1c65c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ac1c65c0ea9c0bb133d68a6ab446341457f5097c))
* Primera versión ([673df27](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/673df274dd32d2da06ccb86ca1c9375ad042ade3))
* Valores default para configuración de certificados ([0c2bd26](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0c2bd268b23138af04b34e075c9d0780e494960a))
* Version nuxt ([72133f9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/72133f9dc7cfbfcce7ef253c54ac32dca765870d))

## [1.14.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.14.0...v1.14.1) (2021-04-16)


### Bug Fixes

* Pluging autenticar ([821c926](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/821c9261d6a02ec236e64bcb7e7d64a7359781e9))

# [1.14.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.13.0...v1.14.0) (2021-04-13)


### Features

* No mostrar brand hasta que cargue la marca ([03d1c8d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/03d1c8decec26693961df8470b950495cf025806))

# [1.13.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.12.1...v1.13.0) (2021-04-13)


### Features

* Tracker 4 Mdq ([082ea67](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/082ea671d747139c8bf57f014f102940a4035696))

## [1.12.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.12.0...v1.12.1) (2021-04-12)


### Bug Fixes

* Guardar datos de usuario ([f1475bb](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f1475bb3d0668b61d65257f968855f59f0ae38b6))
* Guardar datos de usuario ([c86d8c8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c86d8c85e88826b29c88844485c9439693ff4d25))

# [1.12.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.11.0...v1.12.0) (2021-04-12)


### Bug Fixes

* Eliminar consoles ([b83e312](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b83e3124934dfd26104bf434f0a90a60b6772c27))
* Test logo ([d48fc5f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d48fc5ff97bf4f2883d90c614bb9eb79ec74eb6b))
* Test/Proile ([8406735](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8406735cfb2e7809c492d31316643a87b1da7ceb))


### Features

* Agregar RENAPEM a autenticar ([c34698e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c34698e4c51612f4ef849103c2c16d3a5a2fea15))
* Agregar RENAPEM a autenticar ([b4a9726](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b4a9726c36fb584b032270619ba58b3c3c1050b3))
* Husky Version ([2f8d06a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2f8d06ab7b68ad52dcd48837d7d286376d78e93e))

# [1.11.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.10.0...v1.11.0) (2021-03-30)


### Features

* Obtener configuración del brand desde firebase para favicon ([3f88d8e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3f88d8e7f70bddf23a8491da068cd5409d901c5d))

# [1.10.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.9.0...v1.10.0) (2021-03-30)


### Features

* Agregando configuraciones faltantes a brand ([23d121d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/23d121d216874b598cd02de1ff02c0c3799984fd))
* Guardar textos de landing ([2f1237a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2f1237a6179675a2b400615148930729c567343e))

# [1.9.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.8.0...v1.9.0) (2021-03-30)


### Bug Fixes

* Bug en sidebar ([9dec477](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9dec477ba02aa00afac64ffb313703aed34ddead))
* Modificar coleccion desde gcloud funtion ([9714ea1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9714ea16f2f8216c2b08a95b6acce92d321a63bb))


### Features

* Agregar currentUrl ([9245f82](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9245f825220835424f9558af9a2251181438056c))
* CI/CD para berisso ([7627e96](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7627e963fdf942bcc888bbf6a26d8bb5c075a497))
* Envio de email ([7be53f3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7be53f3560ed2cfc54c7cde88857aad23cc893f5))
* Migración de recuperar contraseña y verificar email ([1dbcab5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1dbcab58ce03e0935ecabd66baf3985c46040f31))
* Modificaicón de componenete para poder recuperar la contraseña ([09a9c6a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/09a9c6ae211aa57edf2e1f03941cdab8930da244))

# [1.8.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.7.0...v1.8.0) (2021-03-19)


### Features

* Kick Off Readme ([fc76b2b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fc76b2bc4317dfb9f38a9f0da780adae5c1f4ab8))

# [1.7.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.6.0...v1.7.0) (2021-03-17)


### Bug Fixes

* Actualizar wallet desde perfil ([928dd61](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/928dd61495ca5998da587a416335c475132ad9e4))
* Limpiar campos para csv ([c72a969](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c72a969656e230dbbe7ee5175a3931a68ea44802))
* Mostrar sidebar si el usuario no esta logueado y es mobil ([a2ed213](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a2ed2134400030f1446df9407e5f257f3420a57b))
* Quitar evento no necesario para mostrar sidebar ([84055d0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/84055d00c00b4c5e503f04413796d7063ca04ea9))


### Features

* Add first adapted version of form dni ([b874b2e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b874b2ed5fcf2983c2f83d71a3ba2fd6de46b53a)), closes [#24](https://gitlab.com/oscity/cmp/cmp-enhanced/issues/24)
* Create cultural dni certificate ([2d10925](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2d109259adb4568cc15283319c0c37d9992235d0)), closes [#24](https://gitlab.com/oscity/cmp/cmp-enhanced/issues/24)
* Display sidebar if window with is > 600 ([c2c78f8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c2c78f8664ffd7f1ddd4479b3c897347e083399c))
* Guardar dni de usuario ([5ef069b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5ef069b01bdc5eb7aba2be1600fd030c2568d211))
* Homologar inputs en form ([5deaf56](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5deaf56e8e6a7ea4f1043889d537c8722b67f194))
* Replace title tab "Autenticar" ([7278a58](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7278a58880b6ef008fcd3c02d8d6455a387ec0f0)), closes [#40](https://gitlab.com/oscity/cmp/cmp-enhanced/issues/40)
* Set menu sidebar for citizens ([67e2bc0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/67e2bc08f92e34395bd24609e74166a4021f66b4))

# [1.6.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.5.1...v1.6.0) (2021-03-12)


### Features

* Ocultar tab de 'Sin certificar' y cambio de nombre para tab 'autenticar' ([e0b218c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e0b218cb79395da2b691ee114130271924b9eda3))

## [1.5.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.5.0...v1.5.1) (2021-03-12)


### Bug Fixes

* Update node version to 14 instead 12 in app.yaml ([1e49d6d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1e49d6d96652d14b4bf32c9c85b8fb3997953f8d))

# [1.5.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.4.1...v1.5.0) (2021-03-12)


### Bug Fixes

* Merge Conflicts, fusion Profile Migration ([976a604](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/976a60450e78e29d7428fce07d5283eee221a175))
* Refactor códico menú ([8603cc1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8603cc178367fc436f97cf50587325fd6cc09b7b))
* Validar que el usuario tenga type ([09a69ba](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/09a69bad96c1576697684d89e096a68fcd722d75))


### Features

* Agregar configuración de otros reinos ([2c277f3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2c277f3d781ee38d61c0e0a57410882d7e355bd4))
* Botones para login con otros reinos de autenticar ([ad69541](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ad69541031dcb16d1c4e7bf92a6721b10b7df4d3))
* Editor Update ([a166ee5](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a166ee5422f5087273749ceb14121637941b4fb2))
* Inbox para certificados ([8766d35](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8766d3516cfd8d98095b12658a5f0236fb167b9d))
* Merge ([8285b0d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8285b0df1e003cc04352930cee9ec45cb3522588))
* Merge ([af2e272](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/af2e272b2cb3a5587cfe0af28538a6fe17a8b1f0))
* Migración inbox certificados para administradores ([a6aa6ee](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a6aa6eea322f3ab41ce8540efb4988fbcd939be2))
* Profile with Wallets Atill nedd validations ([ab8f308](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ab8f30820caad05e02f90b624a9bf7f696ed2001))
* Store para configuración de certificados ([699f3b3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/699f3b38f996e5a13bf61f474b7a66443f0ac0b0))
* Uport Integration and Mixins funct for Wallets ([78d5628](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/78d56287970b3ab7b6c44a6776a894f55a239255))
* Validación para acceder a inbox de certificados si eres admin ([f4eea46](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f4eea466667ca38b30aad531095b76db9faf9e95))
* Variables de entorno para reinos de autenticar ([983b20d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/983b20d7385349781d5c0e9e4fdeacf95f880f8a))
* Wallet on profile setted up ([ad3fc82](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ad3fc82c8acad88ffc4f9f536bfbe50512885bd4))

## [1.4.1](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.4.0...v1.4.1) (2021-03-08)


### Bug Fixes

* Guardar confiugración del auth con cloud function al no tener usuario en sesión ([6df7f1c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6df7f1c97c2a41f04630154d14946147de8ac311))
* Guardar textos del landing con cloud function al no tener usuario en sesión ([3c6a9b1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3c6a9b1c0763ca61667cb2a974284866a5140fd5))

# [1.4.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.3.0...v1.4.0) (2021-03-06)


### Bug Fixes

* Centrar mapa y marcador en dirección de usuario ([5b70784](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5b70784271675fb12296a7c403ab99a770920246))
* Update web apge title based in env variable ([4b7d1c1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4b7d1c13956f7d2921b04b2a9c797ba73127e547))


### Features

* Add aditional contact info ([96c5c4b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/96c5c4be5f0c74ea724e63ad5a5e0e05df9f01fc)), closes [#23](https://gitlab.com/oscity/cmp/cmp-enhanced/issues/23)
* Ajustes a header ([4c78c25](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4c78c25b631e3242adf43b2c8e38c213864d76df))
* Avatar componente ([bb7f4c6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bb7f4c6dc58c270f399837d204a7aa75b992c57e))
* Componente para cambio de contraseña ([096288e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/096288e013752838721c1529ac5f760490ff75e6))
* Componente para cambio de email ([3940cee](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3940cee3a10dcef9040e864ac11089b7e43058cd))
* Componente para desactivar cuenta ([d11d0b3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d11d0b31bb3216ecd12fe1b5ed58f92e6a34fabf))
* Configuración para mostrar o no login con autenticar ([e53a9a9](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e53a9a9a8752672cff7b9529ae4f3ce25c523b7a))
* Merge dev ([3480777](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/348077759ccf2a533bbdce58b73112242ffc5b5e))
* Migración componente de mapa ([5827fc8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5827fc819b8fec130769e96e45047231d88c2232))
* Migración edición de perfil ([1c15136](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1c151361a7136b1ef6d769cd007abe76c5964501))
* Migración página de error ([18ca124](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/18ca1240f861a1c3634f29e2eb60417dcda5d1c5))
* Modificación de componente para desactivar cuenta ([c84c9fd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c84c9fde80d742a328751f6b1fe2b29d4cfea20d))
* Modificación en set usuario ([1c8bb72](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1c8bb7227384b7214e3dbc684ff3f634f99eef54))
* Pasar servicios a typescript ([660153e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/660153e1eaf45e30437cf0dfc006ad33ca835694))
* Redirect a perfil y mixin para envío de email ([4b2ee05](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4b2ee056ead8d4382a90f2f243235f25ef95c516))
* Send verification email ([3d2c174](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3d2c1748d0e5290bb3798fd93969e20d48d77c99))
* Servicios para actualizar colección de usuarios ([eb0c588](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/eb0c5880e59e45ffc1dec14cc568b836bafc2a8c))
* Set brand titles and images from firestore ([7387a8a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7387a8a6b62db771913b97fe514044961faf4029)), closes [#23](https://gitlab.com/oscity/cmp/cmp-enhanced/issues/23)
* Textos para i18n, colores para snackbar y filepond ([ae89314](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ae89314150867edff009d447b837fc4996a8f3d6))

# [1.3.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.2.0...v1.3.0) (2021-02-27)


### Features

* Validar usuario de autenticar ([5855642](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5855642086989d8fe2ed5d0eb3b0d984d98ac16d))

# [1.2.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.1.0...v1.2.0) (2021-02-26)


### Features

* Estilos para menú y header ([8dfb36a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8dfb36a95937d0d6d32c47775e06e015a5045457))

# [1.1.0](https://gitlab.com/oscity/cmp/cmp-enhanced/compare/v1.0.0...v1.1.0) (2021-02-26)


### Features

* Ajustes en public components ([f075d53](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f075d539f34fc39e9a30918896966f71fa139047))
* Custom icons ([5885dc0](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5885dc09aef9ee7f87271fdce32f0f5ee9e14a6b))
* Editor Builder Test ([6f2e2a6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6f2e2a666077f8e0b10708a35da4ce78b5c87a4d))
* Estilos landing ([8e8c18f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8e8c18f8133cb71897b921d6bcd71fa40a8567b4))
* Imagenes para landing de demo ([d67691b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d67691b00c57e0afc6b9b00a777115808cef5f5e))
* Middleware authenticated ([7fa0e7e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7fa0e7eac621c285b15ec7d68e1791717634fd95))
* Migración de landing y edición de textos ([5e20017](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5e200176064b5a4c81e03d5cdedede6d07110997))
* Obtener usuario desde collección en firestore ([18c0d4b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/18c0d4bbf59aa6d82719d29f4c71d3848042ffc9))

# 1.0.0 (2021-02-25)


### Bug Fixes

* accents on es version for metamask signature ([3cf312e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3cf312ed5c524dcd2375b72cdbc625d2b329ac94))
* async.d.ts changed to general modules.d.ts ([5c90f67](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5c90f67979c206122e925eb1804be9fbf7914d06))
* async.d.ts changed to general modules.d.ts ([ab4d589](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ab4d5895bba5cee6971fa789fa1678842d48ab3a))
* Brought back Errors client.js ([fc75a0b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fc75a0b7b145d4c62c52307e51697fb5ad6288b1))
* Bye commented code ([576a2c2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/576a2c2b35416b281f13873afb6430a5037f99e5))
* Bye commented code pt2 ([e247727](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e247727ea16e5ca733f54b1ac2f95fdf2a048885))
* Bye console Error Hello Snackbar ([bb06751](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/bb06751e9c260aea79cec40c5c641b644fd1d9c1))
* bye serviceAccount ([045812e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/045812e8f2ed8ba1d02d66e6678c881b026f2baf))
* bye serviceAccount ([fd39ff4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fd39ff482b5d8f945a6b0d82c3b341ba8f4b480e))
* bye serviceAccount ([9e2dfdf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/9e2dfdf7c86ecbef3a63334d671c44fc3827920c))
* bye serviceAccount ([ef7672b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ef7672ba85433d7b73276919a36ae4c1e049fddb))
* Changes from Pam ([79ed987](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/79ed987475cc75bf4124a41806225dbbf771cae2))
* Fix arguments type ([7c394ee](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7c394eee9d6b484b44fa8245f3190871ed6c5dab))
* Mantener sesión activa ([4ceb1f3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4ceb1f308270e4608e41368e55ed7343ec7f0dab))
* mostrar logo si no hay inicio de sesión ([cea2ade](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cea2ade17a53ca7e554de31fa410a88e6359879f))
* mostrar logo si no hay inicio de sesión ([aa6421e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/aa6421eac26b9bf30eeb6ec1adce0a4a62ffd1f8))
* No fields for name and apellideishon on Login ([0e17fd2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0e17fd28c6fa6bdc3c4e7af2ce06de8e05db5ea4))
* No more store/firebase.js ([adeef80](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/adeef8051be5841c15a01416d1e21e03f196d2a8))
* Not vuex error, still need info on AuthData ([3d06929](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3d06929f08286a8f657b88da1d49bf9286506f3a))
* Register test import fixed ([56c0339](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/56c0339f45b4ff38d5c8807fdc07d210042d8975))
* Test imports fixed ([1ab93ce](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1ab93ce1c7daaec0f2dc40a5103d43ea791c742d))
* Test They actually Work ([97e07cf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/97e07cf54c3ad7d612dfc5d76f9ac00e5b5bc85b))


### Features

* captcha-susion-pam ([2eccab6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2eccab6321d8f8e40ed251e8f8a8e8a95448ee56))
* captcha-susion-pam ([701fb81](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/701fb81503b31399bffdba2010b27aabab5cb554))
* Comentar test para validar form ([74b73ef](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/74b73ef4276de3b35827787d982aa6a289a9a455))
* Complementar tests ([845dc8f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/845dc8fadf12aefb4acc69fbc07ef80477c19fdb))
* Complete flow Login + Register WITH EMAIL ([acaab52](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/acaab5214c81509c8430e20020e8fe671d352391))
* componente footer ([a55137a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a55137aaab68f7e7869e98be731796cad6c55acd))
* componentes públicos/generales ([d804878](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/d804878f575c192cb6bfe3301d55a3b857286ff8))
* configuración firebase ([2628d4c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2628d4cf0ae19a5a0d1f1dba03b74beae8680283))
* create register component ([c0c1afd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c0c1afd143ab1add8f92892083c83a551ac3a632))
* create register page ([42a250e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/42a250e458c6c246898a15c9ad285d41bb1624ec))
* Dispatch para set user ([3903ae7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3903ae77a99fbd208a33027cd54302d5d9b40c6f))
* Doble login y logout con autenticar ([fcb27f2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fcb27f243eaf5b35d0a22677fd24b1d29e4e3ec9))
* eliminando código adicional de vue ([a6d087e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a6d087e49bfc9c67d475557eb6b80402a58cf2f9))
* estilos campo phone, importar logo ([3c074fd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/3c074fd5e1b2352da95253217315dbd459ec70d5))
* estilos campo phone, importar logo ([4ba7d57](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4ba7d57685de235e05a9f8183e12d18166ed0941))
* estilos modal registro en header ([e307f30](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/e307f30293aed83aecac84b2d3145066d53c67f8))
* estilos modal registro en header ([4909f4e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4909f4e480ea6e7c2d26984f4bf81c6d1697a005))
* estilos registro y logo ([083e8f7](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/083e8f7a9a456742f858f9fe8f4eb718a96049aa))
* first Commit ([978f4c1](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/978f4c19c116e0e9ead91117b32ce1097e5b670d))
* first-fusion-pam ([6b21676](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/6b216760c05e34c5595d60abffeabb05240f75c4))
* funciones y servicios para registro de usuarios ([c3730c3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c3730c328a9734a976f407d637195ab83ee1893a))
* Guardar usuario en store ([21ce52b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/21ce52b766c78e96b7941a83b3cb2915217be0ee))
* i18n y links para login ([a81bf3a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a81bf3af69138a7880cbf13fd51f38986e9aac71))
* Iconos ([1cc5e5f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1cc5e5f670f8636b2677e528d7e4aa1cc3ab9788))
* imagen-trasera-login ([c7f3393](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c7f3393f0e3d5604d5c20d0e1d9393c864a5fc5f))
* Label with i18n ([fc9db50](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fc9db506b82229aed1135bf961bbf6d81d91c87d))
* Lil fixes ([752d59c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/752d59c4cf1011577048cfee5df89dd6709abe13))
* Login Completed, Start Page builder ([a5bfaab](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a5bfaab818652e1af9bd2d11754888f5d0928b5c))
* Login Wallet still need custom token ([973c30d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/973c30dbc23ed948aa57ed494d99273a08761d7b))
* Login Wallet with Custom Token ([c011041](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c0110413ad5a450c2d92efc17189b89fc4168751))
* Login wqith vuex errors ([8a03e6c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8a03e6c038de0378822f4b8d4d0a384b3c241e9d))
* login-migration ([2d58a8b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/2d58a8ba1d0c7094ba726c9e9163d417d7d876ee))
* Mandar a autenticar solo si se selecciona la opción en login ([0c3273f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/0c3273fc8763c3f1cb6fa1cdaffd1aad0ab76403))
* Mandar a autenticar solo si se selecciona la opción página ([fde5f3f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/fde5f3fd639a918b4289e6f9e131c9d65416c7ff))
* Merge con dev ([b319cf3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b319cf3bea1ce69b715186c232728ef5b585bb30))
* Merge dev ([5d330d4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/5d330d492e66b7ae891de123f48cac2e789a72c7))
* Middleware ([f987031](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f987031bd049acb4e8712b9e5c9d4d104be7b2d3))
* modal de registro en header ([da5de4b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/da5de4b9a171988fe68a8474cd91b9da4b253e5b))
* modal de registro en header ([b36c36e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/b36c36ef9ba1aadc14cd2354d4d64f8bced0feec))
* Mostrar menú al existir un user ([7f16dbf](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7f16dbf96420dd885c16f9221dd61193ef07bf8e))
* Obtención de token de autenticar y login con custom token ([a3d442d](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a3d442dccab9c71a3e39882e80f65af40648206c))
* OnAuthState provisional ([f9a83c2](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f9a83c2594949140d8e238a614b4de24297597fd))
* One component for Login and register Mobile ([04dc85b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/04dc85bcf2415aae1f916989c2a76403063ef6ab))
* package ([f8d12d3](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/f8d12d3453e0df940a19f4b9995d87ad52700469))
* Pam version of store ([7539852](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/7539852e33a97f1c2a2c3b215abe09600f8b1bbc))
* Pruebas tentativas para form builder ([ce99ef8](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ce99ef8d3003be23d98b63a29355427ce7164f1e))
* Pruebas tentativas para form builder ([1cfd6a6](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1cfd6a65fcc08909c4af3d73338289a3ecbcce06))
* Quitar código no utlizado ([95dab86](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/95dab862651c2cd8da5bbac0f0c03d1387bd88f2))
* Quitar comentarios ([4ff020f](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4ff020f4d89b6e9f42dd31efed088de3bb9ab9ce))
* ready for kick off ([272cfb4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/272cfb4b7a79278d7fc96023f42f4e4eaa92f08d))
* ready for kick off ([cf4ed1c](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/cf4ed1c5b0cd2a19829e4c6502cc980c9e529b1b))
* Registro con email ([18d5de4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/18d5de4d58e6911b95ea8229a50788a8667cd752))
* Registro por celular ([8a27c5b](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/8a27c5bd4249c564a0e8fe95d371d3006fcd1e7d))
* second-fusion-pam ([a46da9e](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/a46da9eb5998801d361b3f0922b606d8e271d6ce))
* second-fusion-pam ([1609536](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/1609536d29b47af4dd0f3c8146f8bc9d4ccfedb7))
* Update Login Component ([96fc024](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/96fc024314e2f4ff0ea5272e5f596d7f85d3729b))
* update on eth funcs and i18n ([ae9ac05](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/ae9ac053101cd1b9b061304c9143eec8e456d535))
* update on metamask funcs and i18n ([51c9b2a](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/51c9b2ad9576d791a4aac1cdbaaa773fae2d2e2a))
* Update with firebase nuxt module ([151cde4](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/151cde47517f44c1f578b074450ca88d18e5950e))
* Validar si existe public address ([c2d42dd](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/c2d42dd27b0c78dea506fd357160b78bca04828b))
* Validar si existe public address ([4b130ec](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4b130ec16c7851020e40ea14e3967c24fe9d6ec0))
* wallets para registro ([4d327bb](https://gitlab.com/oscity/cmp/cmp-enhanced/commit/4d327bbcd42c5159591a47478e7738a1511e87a9))
