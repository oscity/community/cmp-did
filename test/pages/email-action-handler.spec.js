import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import axios from 'axios'

import EmailHandler from '@/pages/email-action-handler.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter({
  routes: [
    {
      path: '/email-action-handler',
      name: 'emailHandler',
    },
    {
      path: '/',
      name: 'home',
    },
    {
      path: '/login',
      name: 'login',
    },
  ],
})

jest.mock('axios')

let vuetify
let wrapper

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        doc: jest.fn(() => {
          return {
            get: jest.fn(() => {
              return {
                data: jest.fn().mockReturnValue({}),
              }
            }),
          }
        }),
      }
    }),
  },
}

describe('Email Action Handler Route Default Change', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    router.push({ name: 'login' })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Path goes to home when no mode is given to the switch in mounted', () => {
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
      },
    })
    expect(router.currentRoute.fullPath).toBe('/')
  })
})

describe('resetPassword Functionality Tests', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    router.push({ name: 'login' })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Functionality for resetPassword handler in mounted. Changed password successfuly case.', async () => {
    router.push({ name: 'emailHandler', query: { mode: 'resetPassword' } })
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })

    // Mock the response of axios to return a resolved promise
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: { param: '123123' } })
    )

    // Call the method again with the required data
    await wrapper.vm.handleResetPassword('1234', {
      update: true,
      data: { newPassword: 'contra' },
    })

    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: '1234',
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        password: 'contra',
      },
      uid: '123123',
    })
  })

  it('Functionality for resetPassword handler in mounted. Could not change the password case.', async () => {
    router.push({ name: 'emailHandler', query: { mode: 'resetPassword' } })
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: { param: '123123' } })
    )
    await wrapper.vm.handleResetPassword('1234', {
      update: false,
      data: { newPassword: 'contra' },
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: '1234',
    })
    expect(axios.post).not.toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        password: 'contra',
      },
      uid: '123123',
    })
  })

  it('Functionality for resetPassword handler in mounted. Catch error test.', async () => {
    router.push({ name: 'emailHandler', query: { mode: 'resetPassword' } })
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })
    axios.post.mockImplementation(() =>
      Promise.reject(new Error('Something went wrong'))
    )
    await wrapper.vm.handleResetPassword('1234', {
      update: false,
      data: { newPassword: 'contra' },
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: '1234',
    })
    expect(axios.post).not.toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        password: 'contra',
      },
      uid: '123123',
    })
  })
})

describe('verifyEmail Functionality Tests', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    router.push({ name: 'login' })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Functionality for verifyEmail handler in mounted. Verified email successfuly case.', async () => {
    router.push({
      name: 'emailHandler',
      query: { mode: 'verifyEmail', oobCode: 'c0d3' },
    })
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: { param: '123123' } })
    )
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
        $fire,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })

    await wrapper.vm.handleVerifyEmail('c0d3')

    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: 'c0d3',
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/saveCollectionData', {
      collection: 'users',
      data: { activated: true },
      uid: '123123',
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        emailVerified: true,
      },
      uid: '123123',
    })
    expect(wrapper.vm.title).toBe('¡Gracias!')
    expect(wrapper.vm.message).toBe('Tu cuenta ha sido validada.')
    expect(wrapper.vm.linkToRedirect).toBe('/complete-data?step=3')
  })

  it('Functionality for verifyEmail handler in mounted. Verified email successfuly, but error while getting the user case.', async () => {
    router.push({
      name: 'emailHandler',
      query: { mode: 'verifyEmail', oobCode: 'c0d3' },
    })
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: { param: '123123' } })
    )
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })

    // Mock console error to spy on it. It also omits the errors in console
    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    await wrapper.vm.handleVerifyEmail('c0d3')

    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: 'c0d3',
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/saveCollectionData', {
      collection: 'users',
      data: { activated: true },
      uid: '123123',
    })
    expect(axios.post).toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        emailVerified: true,
      },
      uid: '123123',
    })
    expect(wrapper.vm.title).toBe('¡Gracias!')
    expect(wrapper.vm.message).toBe('Tu cuenta ha sido validada.')
    expect(consoleErrorMock).toHaveBeenCalledTimes(2)

    // Remove the mock from the console for future tests
    consoleErrorMock.mockRestore()
  })

  it('Functionality for verifyEmail handler in mounted. Catch error test for expired token.', async () => {
    router.push({
      name: 'emailHandler',
      query: { mode: 'verifyEmail', oobCode: 'c0d3' },
    })

    // Define the error that the promise will throw
    const err = new Error('Error test - With response')
    err.response = {
      data: {
        name: 'TokenExpiredError',
      },
    }

    // Throw the error so the method enters the catch clause
    axios.post.mockImplementation(() => Promise.reject(err))
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })

    await wrapper.vm.handleVerifyEmail('c0d3')

    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: 'c0d3',
    })
    expect(axios.post).not.toHaveBeenCalledWith(
      'http:link/saveCollectionData',
      {
        collection: 'users',
        data: { activated: true },
        uid: '123123',
      }
    )
    expect(axios.post).not.toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        emailVerified: true,
      },
      uid: '123123',
    })
    expect(wrapper.vm.title).toBe('Ocurrió un error')
    expect(wrapper.vm.message).toBe(
      'El código de acción ya ha caducado. Por favor regresa a tu perfil para reenviarlo.'
    )
  })

  it('Functionality for verifyEmail handler in mounted. Catch error test for other exceptions.', async () => {
    router.push({
      name: 'emailHandler',
      query: { mode: 'verifyEmail', oobCode: 'c0d3' },
    })

    const err = new Error(
      'El código de acción no es válido. Esto puede suceder si el código tiene un formato incorrecto, caducó o ya se ha utilizado.'
    )

    axios.post.mockImplementation(() => Promise.reject(err))
    wrapper = mount(EmailHandler, {
      localVue,
      vuetify,
      router,
      mocks: {
        $t: (msg) => msg,
        axios,
      },
      data() {
        return {
          urlEndpoints: 'http:link',
        }
      },
    })

    await wrapper.vm.handleVerifyEmail('c0d3')

    expect(axios.post).toHaveBeenCalledWith('http:link/validateToken', {
      paramEncode: 'c0d3',
    })
    expect(axios.post).not.toHaveBeenCalledWith(
      'http:link/saveCollectionData',
      {
        collection: 'users',
        data: { activated: true },
        uid: '123123',
      }
    )
    expect(axios.post).not.toHaveBeenCalledWith('http:link/updateUser', {
      data: {
        emailVerified: true,
      },
      uid: '123123',
    })
    expect(wrapper.vm.title).toBe('Ocurrió un error')
    expect(wrapper.vm.message).toBe(err)
  })
})
