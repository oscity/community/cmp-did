import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import NoticeOfPrivacy from '@/pages/notice-of-privacy.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
let vuetify
let wrapper

// eslint-disable-next-line import/no-named-as-default-member
const $store = new Vuex.Store({
  state: {},
  getters: {
    'brand/brand': () => {
      return { privacyPolicy: 'Privacy Policy' }
    },
  },
})

describe('Terms of Service', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('Expect brand to have privacy policy indicated in the store', () => {
    wrapper = mount(NoticeOfPrivacy, {
      localVue,
      vuetify,
      mocks: {
        $store,
        $t: (msg) => msg,
      },
    })
    expect(wrapper.vm.brand.privacyPolicy).toBe('Privacy Policy')
  })
})
