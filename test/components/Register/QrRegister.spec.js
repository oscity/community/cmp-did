import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import QrRegister from '~/components/Register/QrRegister.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

// eslint-disable-next-line import/no-named-as-default-member
const $store = new Vuex.Store({
  state: {},
  getters: {
    'users/user': () => {
      return {
        first_name: 'Pepe',
        last_name: 'Aguilar',
        email: 'joker@email.com',
        password: '****',
        dni: '234546',
        public_address: '',
        type: 'superadmin',
        uid: '97sd9d',
      }
    },
    'brand/logo': () => {
      return 'url'
    },
  },
})

describe('Test QrRegister functionalities', () => {
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('The resetApp method should clean the address data and call the method walletConnectInit', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },

      data() {
        return {
          address: '10dxkjs7dh3o',
        }
      },
    })

    const walletConnectInitSpy = jest.spyOn(wrapper.vm, 'walletConnectInit')

    wrapper.vm.resetApp()
    expect(walletConnectInitSpy).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.address).toBe('')
  })

  it('When the Wallet is disconnected, it should called the method onDisconnect and this should call the resetApp method', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },

      data() {
        return {
          address: '10dxkjs7dh3o',
        }
      },
    })

    const resetAppSpy = jest.spyOn(wrapper.vm, 'resetApp')

    wrapper.vm.onDisconnect()
    expect(resetAppSpy).toHaveBeenCalledTimes(1)
  })

  it('When the Wallet is connected, it should called the method onConnect which starts the timer and emits the user with the public address', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    const testUser = {
      first_name: 'Pepe',
      last_name: 'Aguilar',
      email: 'joker@email.com',
      password: '****',
      dni: '234546',
      public_address: '10dxkjs7dh3o',
      type: 'superadmin',
      uid: '97sd9d',
    }

    wrapper.vm.onConnect({ params: [{ accounts: ['10dxkjs7dh3o'] }] })

    expect(wrapper.vm.fullUser).toStrictEqual(testUser)
    expect(wrapper.emitted().certificateData).toBeTruthy()
    expect(wrapper.emitted().certificateData[0][0]).toBe('10dxkjs7dh3o')
  })

  it('When the display is xl, the computed property widthGooglePlay should return 60%', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    wrapper.vm.$vuetify.breakpoint.name = 'xl'

    expect(wrapper.vm.widthGooglePlay).toBe('60%')
  })

  it('When the display is lg, the computed property widthGooglePlay should return 80%', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    wrapper.vm.$vuetify.breakpoint.name = 'lg'

    expect(wrapper.vm.widthGooglePlay).toBe('80%')
  })

  it('When the display not xl nor lg, the computed property widthGooglePlay should return 100%', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    wrapper.vm.$vuetify.breakpoint.name = 'md'

    expect(wrapper.vm.widthGooglePlay).toBe('100%')
  })

  it('When the address watcher detects that the value of the address is not empty, it emits shotSteps as false', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    wrapper.setData({ address: '10dxkjs7dh3o' })

    wrapper.vm.$options.watch.address.call(wrapper.vm, wrapper.vm.address)

    expect(wrapper.emitted().showSteps).toBeTruthy()
    expect(wrapper.emitted().showSteps[0][0]).toBe(false)
  })

  it('When the address watcher detects that the value of the address is empty, it emits shotSteps as true', async () => {
    const wrapper = await mount(QrRegister, {
      vuetify,
      localVue,

      mocks: {
        $t: (msg) => msg,
        $store,
      },
    })

    wrapper.setData({ address: '' })

    wrapper.vm.$options.watch.address.call(wrapper.vm, wrapper.vm.address)

    expect(wrapper.emitted().showSteps).toBeTruthy()
    expect(wrapper.emitted().showSteps[0][0]).toBe(true)
  })
})
