import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'

import Vuetify from 'vuetify'
import MisionesFondoDeCredito from '~/components/Certificates/MisionesFondoDeCredito.vue'
import { state, mutations, getters } from '~/store/certsConfig.js'

const localVue = createLocalVue()
localVue.use(Vuex)

const $i18n = {
  locale: 'es',
}

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        add: jest.fn(),
      }
    }),
  },
}

const $fireModule = {
  storage: jest.fn(() => {
    return {
      ref: jest.fn(() => {
        return {
          child: jest.fn(() => {
            return {
              put: jest.fn(),
            }
          }),
        }
      }),
    }
  }),
}

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    certsConfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

describe('Check MisionesFondoDeCredito functionalities', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Clicking the Cancel button emits to close the dialog', async () => {
    const FilePondStub = {
      render: () => {},
      methods: {
        removeFiles: () => true,
      },
    }

    wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: FilePondStub },
    })

    const spyCancel = jest.spyOn(wrapper.vm, 'closeDialog')
    const spyClear = jest.spyOn(wrapper.vm, 'clearData')

    const cancelBtn = wrapper.find('#cancelBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spyCancel).toHaveBeenCalledTimes(1)
    expect(spyClear).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().closeDialog).toBeTruthy()
  })

  it('The updateFiles method asigns the name correctly from the file uploaded from the FilePond component', async () => {
    const wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateFiles = jest.spyOn(wrapper.vm, 'updateFiles')

    const filesMock = [{ file: { name: 'fileName' } }]

    await wrapper.vm.updateFiles(filesMock)

    expect(spyUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.files).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.pdf).toBe('fileName')
  })

  it('The updateFiles method asigns empty filename if no name is found', async () => {
    const wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateFiles = jest.spyOn(wrapper.vm, 'updateFiles')

    const filesMock = []

    await wrapper.vm.updateFiles(filesMock)

    expect(spyUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.files).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.pdf).toBe('')
  })

  it('The errorUpdateFiles method, valid file size and asigns false to valid form', async () => {
    const wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })
    const spyErrorUpdateFiles = jest.spyOn(wrapper.vm, 'errorUpdateFiles')
    const filesMock = [{ file: { size: 15554049 } }]

    await wrapper.vm.errorUpdateFiles(filesMock)

    expect(spyErrorUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.valid).toBeFalsy()
  })

  it('Method "getFechaPrimeraCuota" calculates the date correctly', async () => {
    wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      data() {
        return {
          dateFinPG: '2021-04-20',
        }
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    await wrapper.vm.getFechaPrimeraCuota()

    expect(wrapper.vm.pagoPrimeraCuota).toBe('2021-05-20')
  })

  it('Method "createNotifications" creates and writes the notifications to firestore', async () => {
    wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },

      data() {
        return {
          dateFinPG: '2021-04-20',
          certificado: {
            firstName: 'Ben',
            lastName: 'Tennison',
            date: '2021-06-09',
            email: 'ben10@hotmail.com',
          },
        }
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    await wrapper.vm.createNotifications()

    expect($fire.firestore.collection).toHaveBeenCalledTimes(3)
  })

  it('Clicking the Save Button saves the info correctly', async () => {
    wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            firstName: 'Ben',
            lastName: 'Tennison',
            dni: '2222',
            cuit: '3333',
            date: '2021-06-09',
            address: 'San Diego',
            locality: 'Por alla',
            pdf: 'lorem-ipsum.pdf',
            email: 'ben10@hotmail.com',
          },
          plazo_total: '9',
          dateFinPG: '2021-06-09',
          pagoPrimeraCuota: '2021-07-09',
          files: [{ file: { name: '' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)
  })

  it('Catch test for the Save Button', async () => {
    wrapper = mount(MisionesFondoDeCredito, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            firstName: 'Ben',
            lastName: 'Tennison',
            dni: '2222',
            cuit: '3333',
            date: '2021-06-09',
            address: 'San Diego',
            locality: 'Por alla',
            pdf: 'lorem-ipsum.pdf',
            email: 'ben10@hotmail.com',
          },
          plazo_total: '9',
          dateFinPG: '2021-06-09',
          pagoPrimeraCuota: '2021-07-09',
          files: [{ file: { name: '' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.emitted().closeDialog).toBeTruthy()
    expect(wrapper.emitted().messages).toBeTruthy()
  })
})
