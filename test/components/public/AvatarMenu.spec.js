import Vuetify from 'vuetify'
import Vuex, { Store } from 'vuex'
import VueRouter from 'vue-router'
import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import axios from 'axios'
import AvatarMenu from '@/components/public/AvatarMenu.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
document.body.setAttribute('data-app', true)

const emptyUser = {
  first_name: '',
  last_name: '',
  email: '',
  password: '',
  dni: '',
  public_address: '',
}
const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))
// Mock de localStorage
const localStorageMock = (function () {
  let store = {}

  return {
    getItem(key) {
      return store[key]
    },

    setItem(key, value) {
      store[key] = value
    },

    clear() {
      store = {}
    },

    removeItem(key) {
      delete store[key]
    },

    getAll() {
      // eslint-disable-next-line no-console
      console.log(store)
    },
  }
})()

Object.defineProperty(window, 'localStorage', { value: localStorageMock })

const createStore = (user, isDID, isLoggedIn) =>
  new Store({
    state: { loggedIn: isLoggedIn || false },

    getters: {
      'users/user': () => {
        return {
          ...emptyUser,
          ...user,
        }
      },
      'authconfig/isDID': () => {
        return isDID || false
      },
    },
    actions: {
      'users/fetchUser': () => {
        return jest.fn()
      },
      setloggedIn: () => {
        return jest.fn()
      },
    },
  })

jest.mock('axios')
describe('Testing Avatar Menu', () => {
  let vuetify
  let wrapper
  let store

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  afterEach(() => {
    wrapper.destroy()
    window.localStorage.clear()
  })

  test('should mount basic components succesfully', () => {
    store = createStore()

    wrapper = mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
    })
    expect(wrapper.find('.v-menu').exists()).toBe(true)
    expect(wrapper.find('.v-avatar').exists()).toBe(true)
  })

  test('should display menu if AvatarMenu is clicked', async () => {
    store = createStore()

    wrapper = mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
    })
    await wrapper.find('.v-btn').trigger('click')
    expect(wrapper.find('.v-card').exists()).toBe(true)
  })
  test('should mount "Identidad Digital" & "Portadocumentos" buttons if (instance is DID) and (user is logged in and is not verified)', async () => {
    const user = {
      first_name: 'Ruperto',
      last_name: 'Vega',
    }

    store = createStore(user, true)
    wrapper = mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({ userIsRegistered: true, userHavePublicAddress: true })
    await wrapper.vm.$nextTick()

    await wrapper.find('.v-btn').trigger('click')
    expect(wrapper.find('[data-test="digitalIdentity"]').exists()).toBe(true)
    expect(wrapper.find('[data-test="wallet"]').exists()).toBe(true)
  })

  test('should mount "Complete Register" button if user is logged in and is not verified', async () => {
    const user = {
      first_name: 'Ruperto',
      last_name: 'Vega',
      uid: '12345678',
    }
    const mocks = {
      axios,
      $t: (msg) => msg,
    }
    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: { user: { email: '', providerData: [] } },
      })
    )

    store = createStore(user, true, true)
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })

    await wrapper.vm.$nextTick()

    await wrapper.find('.v-btn').trigger('click')
    expect(wrapper.find('[data-test="digitalIdentity"]').exists()).toBe(false)
    expect(wrapper.find('[data-test="wallet"]').exists()).toBe(false)
    expect(wrapper.find('[data-test="completeRegister"]').exists()).toBe(true)
  })
  test('should shorten the username if it is longer than 20 characters', () => {
    const user = {
      first_name: 'Uvuvwevwevwe Onyetenyevwe',
      last_name: 'Ugwemuhwem Osas',
    }

    store = createStore(user, true)
    wrapper = mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.vm.userName).toMatch('Uvuvwevwevwe Ugwem...')
  })
  test('should verify that the user has a registered email if he has a uid', async () => {
    const user = {
      uid: '20301912928',
    }
    store = createStore(user)
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    const spy = jest.spyOn(wrapper.vm, 'verifyUserRegistration')
    wrapper.vm.$options.watch.user.call(wrapper.vm, user)
    expect(spy).toHaveBeenCalled()
  })
  test("'Identidad Digital' button should direct to certificate if user have it ", async () => {
    const user = {
      uid: '20301912928',
    }
    const idReference = 'UdBC96OF1NyJPpcdzyup'
    store = createStore(user, true)
    const mocks = {
      $fire: {
        firestore: {
          collection: jest.fn(() => {
            return {
              where: jest.fn(() => {
                return {
                  where: jest.fn(() => {
                    return {
                      limit: jest.fn(() => {
                        return {
                          get: jest.fn(() =>
                            Promise.resolve({
                              docs: [
                                {
                                  data: () => {
                                    return {
                                      id_reference: idReference,
                                      link: `idReference/adsasd/${idReference}`,
                                    }
                                  },
                                },
                              ],
                            })
                          ),
                        }
                      }),
                    }
                  }),
                }
              }),
            }
          }),
        },
      },
    }
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({ userIsRegistered: true, userHavePublicAddress: true })
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="digitalIdentity"]').trigger('click')
    expect(router.currentRoute.path).toBe(
      `/citizen/certificates/digital-identity/${idReference}`
    )
    // expect(wrapper.find('[data-test="digitalIdentity"]').exists()).toBe(true)
  })
  test("'Identidad Digital' button should direct to create a certificate if user don't have it ", async () => {
    const user = {
      uid: '20301912928',
    }
    // const idReference = ''
    store = createStore(user, true)
    const mocks = {
      $fire: {
        firestore: {
          collection: jest.fn(() => {
            return {
              where: jest.fn(() => {
                return {
                  where: jest.fn(() => {
                    return {
                      limit: jest.fn(() => {
                        return {
                          get: jest.fn(() =>
                            Promise.resolve({
                              docs: [
                                {
                                  data: () => {
                                    return null
                                    // return {
                                    //   id_reference: idReference,
                                    // }
                                  },
                                },
                              ],
                            })
                          ),
                        }
                      }),
                    }
                  }),
                }
              }),
            }
          }),
        },
      },
    }
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      userIsRegistered: true,
      userHavePublicAddress: true,
    })
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="digitalIdentity"]').trigger('click')
    expect(router.currentRoute.path).toBe(
      `/citizen/certificates/digital-identity/`
    )
  })
  test("Logout button should logout if current route is not '/complete-data'", async () => {
    const user = {
      uid: '20301912928',
    }
    const mocks = {
      $fire: {
        auth: {
          signOut: jest.fn(),
        },
      },
    }

    const store = createStore(user, true)
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      userIsRegistered: true,
      userHavePublicAddress: true,
    })
    const spy = jest.spyOn(wrapper.vm, 'logout')
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="logout"]').trigger('click')
    expect(spy).toHaveBeenCalled()
  })

  // TODO:
  test("If current route is '/complete-data', Logout button should redirect to '/' and a swal will be ejecuted", async () => {
    const user = {
      uid: '20301912928',
    }
    const mocks = {
      $fire: {
        auth: {
          signOut: jest.fn(),
        },
      },
    }
    await router.push('/complete-data')
    const store = createStore(user, true)
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      userIsRegistered: true,
      userHavePublicAddress: true,
    })
    // wrapper.vm.$root.$on = { logout: true }
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="logout"]').trigger('click')
    expect(router.currentRoute.path).toBe('/')
  })

  // TODO: Check $root.$on branch (lines 245-246) on logoutIf Method

  // TODO: Logout remove localstorage data ()
  test("On logout, if 'reino' is on localStorage should remove it", async () => {
    const user = {
      uid: '20301912928',
    }

    const mocks = {
      $fire: {
        auth: {
          signOut: jest.fn(),
        },
      },
      $autenticar: {
        logout: jest.fn(() => Promise.resolve({ data: {} })),
      },
    }
    window.localStorage.setItem('reino', 'r31n0')
    window.localStorage.setItem('cuit', 'cu1t')
    window.localStorage.setItem('backToReino', 'backToRe1n0')
    window.localStorage.setItem('levelUpdated', 'l3v3lUpdat3d')
    const store = createStore(user, true)
    await router.push('/login')
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      userIsRegistered: true,
      userHavePublicAddress: true,
    })
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="logout"]').trigger('click')
    await flushPromises()
    expect(mocks.$autenticar.logout).toBeCalledTimes(1)
    expect(localStorage.getItem('reino')).toBeUndefined()
    expect(localStorage.getItem('backToReino')).toBeUndefined()
    expect(localStorage.getItem('cuit')).toBeUndefined()
  })
  // TODO: Catch error on Logout
  test('On logout catch error if there is some error on logOut (Firebase)', async () => {
    const user = {
      uid: '20301912928',
    }
    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    const mocks = {
      $autenticar: {
        logout: jest.fn(() => Promise.resolve({ data: {} })),
      },
    }
    window.localStorage.setItem('reino', 'r31n0')
    window.localStorage.setItem('cuit', 'cu1t')
    window.localStorage.setItem('backToReino', 'backToRe1n0')
    window.localStorage.setItem('levelUpdated', 'l3v3lUpdat3d')
    const store = createStore(user, true)
    await router.push('/login')
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      userIsRegistered: true,
      userHavePublicAddress: true,
    })
    await wrapper.vm.$nextTick()
    await wrapper.find('.v-btn').trigger('click')
    await wrapper.find('[data-test="logout"]').trigger('click')
    await flushPromises()
    expect(consoleErrorMock).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe('Ocurrió un error al cerrar sesión')
    expect(wrapper.vm.snackbar.color).toBe('error')
    consoleErrorMock.mockRestore()
  })
  test('On verifyUserRegistration catch error if there is some error on Cloud Function', async () => {
    const user = {
      uid: '20301912928',
      public_address: 'jguyoidasd123123',
    }
    const mocks = {
      axios,
      $autenticar: {
        logout: jest.fn(() => Promise.resolve({ data: {} })),
      },
      $t: (msg) => msg,
    }
    axios.post.mockImplementation(() =>
      Promise.reject(new Error('Something went wrong'))
    )

    const store = createStore(user, true)
    wrapper = await mount(AvatarMenu, {
      localVue,
      vuetify,
      store,
      router,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.userIsRegistered).toBe(false)
    expect(wrapper.vm.userHavePublicAddress).toBe(true)
  })
})
