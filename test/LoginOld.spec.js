import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Login from '@/components/Login/Login.vue'

describe('Login', () => {
  const localVue = createLocalVue()
  const $t = () => {}
  let vuetify
  let wrapper
  let email
  let password

  beforeEach(() => {
    vuetify = new Vuetify()
    wrapper = shallowMount(Login, {
      localVue,
      vuetify,
      mocks: {
        $t,
      },
    })
    email = wrapper.find('#login_email')
    password = wrapper.find('#login_password')
  })

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })

  test('Read inputs and variables to exists and be empty on mounting', () => {
    expect(email.text()).toBe('')
    expect(password.text()).toBe('')
    expect(wrapper.vm.$data.email).toBe('')
    expect(wrapper.vm.$data.password).toBe('')
  })
  // test('Check Input types and fields not empty before submitting', async () => {
  //   wrapper.vm.$data.email = 'ivan@os.city'
  //   wrapper.vm.$data.password = 'holiholi'
  //   await email.setValue(wrapper.vm.$data.email)
  //   await password.setValue(wrapper.vm.$data.password)
  //   expect(email.element.value).toContain('@')
  //   expect(password.element.value).toMatch('holiholi')
  // })

  //   test('Should disable the button if Form fields R empty', async () => {
  //     const emailText = ''
  //     console.log('email')
  //     console.log(email)
  //     if (email) await email.setValue(emailText)
  //     expect(wrapper.find('#save-user').attributes('disabled')).toBeTruthy()
  //   })
})

/* Test Logo */
// import { createLocalVue, mount } from '@vue/test-utils'
// import Vuetify from 'vuetify'
// import Vuex from 'vuex'

// import Logo from '@/components/public/Logo.vue'
// import { state, mutations, getters } from '~/store/brand'

// describe('Logo', () => {
//   const localVue = createLocalVue()
//   localVue.use(Vuex)
//   let vuetify
//   let wrapper
//   const store = new Vuex.Store({
//     state,
//     mutations,
//     getters,
//   })

//   beforeEach(() => {
//     vuetify = new Vuetify()
//     wrapper = mount(Logo, {
//       localVue,
//       vuetify,
//       propsData: {
//         heightImage: '48px',
//       },
//       store,
//     })
//   })

//   test('is a Vue instance', () => {
//     expect(wrapper.vm).toBeTruthy()
//   })
// })
