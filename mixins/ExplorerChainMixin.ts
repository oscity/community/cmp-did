import Vue from 'vue'

export default Vue.extend({
  name: 'ExplorerChainMixin',
  data() {
    return {}
  },
  methods: {
    get_tx_lookup_chain(chain: string, txid: string) {
      if (chain === 'bitcoinTestnet') {
        return 'https://live.blockcypher.com/btc-testnet/tx/' + txid
      } else if (chain === 'bitcoinMainnet') {
        return 'https://blockchain.info/tx/' + txid
      } else if (chain === 'bitcoinRegtest' || chain === 'mockchain') {
        return 'This has not been issued on a blockchain and is for testing only'
      } else if (chain === 'ethereumMainnet') {
        return 'https://etherscan.io/tx/' + txid
      } else if (chain === 'ethereumRopsten') {
        return 'https://ropsten.etherscan.io/tx/' + txid
      } else if (chain === 'rskMainnet') {
        return 'https://explorer.rsk.co/tx/' + txid
      } else if (chain === 'rskTestnet') {
        return 'https://explorer.testnet.rsk.co/tx/' + txid
      } else if (chain === 'bfaMainnet') {
        return 'http://bfascan.com.ar/tx/' + txid
      } else if (chain === 'bfaTestnet') {
        return 'http://bfascan.com.ar/tx/' + txid
      } else if (chain === 'lacchainMainnet') {
        return 'https://explorer.lacchain.net/tx/' + txid
      } else if (chain === 'lacchainTestnet') {
        return 'http://34.123.38.34/explorer/tx/' + txid
      } else if (chain === 'etcMainnet') {
        return 'https://gastracker.io/tx/' + txid
      } else if (chain === 'etcTestnet') {
        return 'https://kottiexplorer.ethernode.io/tx/' + txid
      }
    },
  },
})
