import Vue from 'vue'
import moment from 'moment'
import axios from 'axios'
import * as Papa from 'papaparse'
import { mapGetters } from 'vuex'
import { Certificate } from 'oscity-cert-verifier-js'

import {
  updateCertificate,
  setCertificate,
  deleteCertificate,
  getCertificatesByUser,
} from '@/services/Certificates'
import { saveFile } from '@/services/Firebase'

export default Vue.extend({
  name: 'CertificateMixin',
  data() {
    return {
      data: Object(),
      verificationSteps: [] as any,
      verificationResult: Object(),
    }
  },
  computed: {
    ...mapGetters({
      user: 'users/user',
    }),
  },
  methods: {
    createCsv(element: any, collectionName: string) {
      const fullData = []
      delete element.is_certified
      delete element.estado_origen
      delete element.is_revocated
      fullData.push(element)
      const i = Papa.unparse(fullData)
      const file = new Blob(['\uFEFF', i], {
        type: 'data:text/csv;charset=utf-8',
      }) as any
      const date = moment().format('YYYY_MM_DD_HH_mm')
      file.name = `certificates_${date}.csv`
      /** TODO: Remove when clean cert-viewer */
      const storageName =
        collectionName === 'certificatesContributor'
          ? 'certificates_contributor'
          : collectionName
      saveFile(this.$fireModule, file, storageName)
    },
    async certificateData(
      collectionName: string,
      certificate: Object
    ): Promise<void> {
      this.data = {
        ...certificate,
        estado_origen: 'En espera',
        created: moment().format(),
        levelCidi: this.user.levelCidi,
        is_certified: false,
        is_revocated: false,
        n_cuit: this.user.dni,
      }
      delete this.data.link
      delete this.data.id_reference
      const idCert = await setCertificate(this.$fire, this.data, collectionName)
      this.data.id_reference = idCert.id
      await updateCertificate(this.$fire, idCert.id, this.data, collectionName)
      this.createCsv(this.data, collectionName)
    },
    certificateInfo(certificate: any): Object {
      return {
        '@context': certificate['@context'],
        badge: certificate.badge,
        created: certificate.created,
        displayHtml: certificate.displayHtml,
        dni: certificate.dni,
        email: certificate.email,
        id: certificate.id,
        id_reference: certificate.id_reference,
        id_user: certificate.id_user,
        issuedOn: certificate.issuedOn,
        levelCidi: certificate.levelCidi,
        n_cuit: certificate.n_cuit,
        name_citizen: certificate.name_citizen,
        public_address: certificate.public_address,
        recipient: certificate.recipient,
        recipientProfile: certificate.recipientProfile,
        signature: certificate.signature,
        title: certificate.title,
        type: certificate.type,
        verification: certificate.verification,
      }
    },
    async verify(data: Object) {
      const certificateInfo = this.certificateInfo(data)
      let url = ''
      if (process.env.NODE_ENV !== 'development') {
        url = `${process.env.OSCITY_ENDPOINTS_URL}/parsing-ethereum-rpcparsingfunction`
      } else {
        url =
          'http://localhost:5001/mar-de-plata/us-central1/parsing-ethereum-rpcparsingfunction'
      }
      const certificate = new Certificate(certificateInfo, {
        explorerAPIs: [
          {
            priority: 1,
            apiType: 'rpc',
            async parsingFunction(conf: any) {
              const res = await axios.post(url, conf)
              return res.data
            },
            serviceURL: 'http://35.239.55.127/nodo/',
          },
        ],
        locale: 'es-MX',
      })
      await certificate.init()
      const verificationResult = await certificate.verify(
        ({
          code,
          label,
          status,
          errorMessage,
        }: {
          code: string
          label: string
          status: string
          errorMessage: string
        }) => {
          // console.log('Code:', code, label, ' - Status:', status)
          if (errorMessage) {
            console.error(
              `The step ${code} fails with the error: ${errorMessage}`
            )
          }
          this.verificationSteps.push({ code, label, status, errorMessage })
        }
      )
      this.verificationResult = verificationResult
      if (verificationResult.status === 'failure') {
        console.error(
          `The certificate is not valid. Error: ${verificationResult.message}`
        )
      }
    },
    async deleteCertificatesByUser(userUid: string, collection: string) {
      await getCertificatesByUser(this.$fire, userUid, collection).then(
        (certs: any) => {
          certs.forEach(async (doc: any) => {
            await deleteCertificate(this.$fire, doc.id)
          })
        }
      )
    },
  },
})
