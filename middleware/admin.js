import _ from 'lodash'

export default function ({ store, redirect }) {
  if (store.state.loggedIn && !isAdmin(store.state.users.user)) {
    return redirect('/forbidden')
  }
}

function isAdmin(user) {
  if (user && user.type && _.includes(['superadmin', 'operator'], user.type)) {
    return true
  }
  return false
}
