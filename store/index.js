export const state = () => ({
  user: {
    user: {},
  },
  autenticarUser: {},
  locale: 'es',
  isHomeTop: true,
  currentUrl: '',
  loggedIn: false,
})

export const mutations = {
  ON_AUTH_STATE_CHANGED_MUTATION: (state, { authUser }) => {
    // , _claims }) => {
    if (!authUser) {
      // claims = null
      // perform logout operations
      state.user = {
        user: {},
      }
      state.loggedIn = false
    } else {
      const { uid, email, emailVerified, phoneNumber } = authUser
      state.user = { uid, email, emailVerified, phoneNumber }
      state.loggedIn = true
    }
  },
  SET_LOCALE(state, locale) {
    state.locale = locale
  },
  SET_HOME_TOP(state, isTop) {
    state.isHomeTop = isTop
  },
  SET_HOST(state, host) {
    state.currentUrl = host
  },
  SET_AUTENTICAR(state, { autenticar }) {
    state.autenticarUser = { ...state.autenticarUser, ...autenticar }
  },
  SET_LOGGEDIN(state, loggedIn) {
    state.loggedIn = loggedIn
  },
}

export const actions = {
  onAuthStateChangedAction: ({ commit }, { authUser }) => {
    commit('ON_AUTH_STATE_CHANGED_MUTATION', { authUser })
  },
  changeLocale({ commit }, locale) {
    commit('SET_LOCALE', locale)
  },
  setUserAutenticar({ commit }, { autenticar }) {
    commit('SET_AUTENTICAR', { autenticar })
  },
  setloggedIn({ commit }, loggedIn) {
    commit('SET_LOGGEDIN', loggedIn)
  },
  setHost({ commit }) {
    const currentUrl = `${window.location.protocol}//${window.location.host}`
    commit('SET_HOST', currentUrl)
  },
}

export const getters = {
  loggedIn(state) {
    return state.loggedIn
  },
  isHomeTop(state) {
    return state.isHomeTop
  },
  currentUrl(state) {
    return state.currentUrl
  },
  autenticarUser(state) {
    return state.autenticarUser
  },
}
