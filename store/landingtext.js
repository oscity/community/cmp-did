import axios from 'axios'
import { getLastLandingText } from '~/services/LandingText'

export const state = () => ({
  landingtext: {
    heroSection: {
      title: '',
      subtitle: '',
      second_subtitle: '',
    },
    headerSection: {
      login_button: 'Iniciar sesión',
      register_button: 'Registro',
    },
    columnsSection: {
      city: '',
      slogan: '',
      title_column1: '',
      title_column2: '',
      title_column3: '',
      info_column1: '',
      info_column2: '',
      info_column3: '',
    },
    reminderSection: {
      reminderTitle: '',
      reminderText: '',
    },
    certificateSection: {
      certificate_title: '',
    },
    digitalCitizen: {
      title: '',
      subtitle: '',
      levels_description: '',
      step_1: {
        description: '',
      },
      step_2: {
        description: '',
      },
      step_3: {
        description: '',
      },
      step_4: {
        description: '',
      },
      step_5: {
        description: '',
      },
    },
    whatIsCidi: {
      section_1: {
        title: '',
        description: '',
      },
      section_2: {
        title: '',
        description: '',
      },
      section_3: {
        title: '',
        description: '',
      },
      section_4: {
        title: '',
        description: '',
      },
    },
    step_number_color: '',
    loaded: false,
  },
})

export const mutations = {
  SET_LANDINGTEXT(state, landingtext) {
    state.landingtext = { ...state.landingText, ...landingtext }
  },
}

export const actions = {
  async get({ commit, rootState }) {
    await getLastLandingText(this.$fire).then(async (result) => {
      const landingText = rootState.landingtext.landingtext
      if (!result.empty) {
        const data = {
          ...result.docs[0].data(),
          id: result.docs[0].id,
        }
        commit('SET_LANDINGTEXT', { ...landingText, ...data })
      } else {
        const urlEndpoints = process.env.OSCITY_ENDPOINTS_URL
        await axios.post(`${urlEndpoints}/saveCollectionData`, {
          collection: 'landingText',
          data: landingText,
        })
      }
    })
    commit('SET_LANDINGTEXT', {
      ...rootState.landingtext.landingtext,
      loaded: true,
    })
  },
}

export const getters = {
  landingtext: (state) => {
    return state.landingtext
  },
}
