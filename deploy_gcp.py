import subprocess
import sys
from string import Template

from loguru import logger

if len(sys.argv) < 3:
    raise ValueError(
        'Missing arguments <google_credentials_file> <project_id>')

key_file = sys.argv[1]
project_id = sys.argv[2]

# Google Project configuration
# Activate sevice account
config_gcloud = Template(
    "gcloud auth activate-service-account --key-file $KEY_FILE && gcloud config set project $PROJECT_ID").substitute(KEY_FILE=key_file, PROJECT_ID=project_id)
logger.info(config_gcloud)
subprocess.run(config_gcloud, shell=True, check=True)

# Enable services (Google API)
subprocess.run(
    'gcloud services enable storage.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable cloudbuild.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable iamcredentials.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable maps-android-backend.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable maps-backend.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable maps-embed-backend.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable maps-ios-backend.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable places-backend.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable geocoding-backend.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable deploymentmanager.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable dns.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable cloudfunctions.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable language.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable oslogin.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable translate.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable vision.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable compute.googleapis.com --async', shell=True, check=True)
# subprocess.run(
#     'gcloud services enable computescanning.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable firebaseextensions.googleapis.com --async', shell=True, check=True)
subprocess.run(
    'gcloud services enable firestore.googleapis.com --async', shell=True, check=True)

# Deploy app
subprocess.run('yarn deploy-prod', shell=True, check=True)
