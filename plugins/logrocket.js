import LogRocket from 'logrocket'
import * as Sentry from '@sentry/browser'

export default function (context) {
  if (context.env.NODE_ENV === 'production') {
    LogRocket.init(context.env.LOGROCKET_ID)
    if (context.store.state.users.user) {
      LogRocket.identify(context.store.state.users.user.uid, {
        name: `${context.store.state.users.user.first_name} ${context.store.state.users.user.last_name}`,
        email: context.store.state.users.user.email,
        cuit: context.store.state.users.user.dni,
      })
    }
    LogRocket.getSessionURL((sessionURL) => {
      Sentry.configureScope((scope) => {
        scope.setExtra('sessionURL', sessionURL)
      })
    })
  }
}
